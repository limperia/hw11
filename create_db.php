<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/connect.php';


try{ 
    $sql ='CREATE TABLE members(
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        full_name VARCHAR(255),
        phone VARCHAR(255),
        email VARCHAR(255),
        role VARCHAR(255),
        averange_mark FLOAT(10,1),
        subject VARCHAR(255),
        working_day VARCHAR(255)
        )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;';
      $connection->exec($sql); 
}

catch(Exception $exerror){ 
echo 'Ошибка при создании таблицы';
echo $exerror -> getMessage();
die();
}
echo 'Таблица создалась успешно';
die();


?>