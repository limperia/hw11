<?php

$array_members = [
    [
        'full_name' => 'Megan Adelle',
        'phone' => '050-454-58-89',
        'email' => 'irey@gmail.ru',
        'role' => 'Student',
        'averange_mark' => 8.5
        

    ],
    [
        'full_name' => 'Tom Piterson',
        'phone' => '099-454-06-02',
        'email' => 'ty@gmail.ru',
        'role' => 'Student',
        'averange_mark' => 6.5
    

    ],
    [
        'full_name' => 'Rose Abdoo',
        'phone' => '050-454-40-22',
        'email' => 'tety@gmail.ru',
        'role' => 'Student',
        'averange_mark' => 7.8
       
    ],
    [
        'full_name' => 'Whitney Avalon',
        'phone' => '099-444-66-33',
        'email' => 'tylop@gmail.ru',
        'role' => 'Student',
        'averange_mark' => 6.9
      

    ],
    [
        'full_name' => 'Mili Avital',
        'phone' => '098-774-57-77',
        'email' => 'crewy@gmail.ru',
        'role' => 'Student',
        'averange_mark' => 8.5
        

    ],
    [
        'full_name' => 'Christina Aguilera',
        'phone' => '050-111-54-19',
        'email' => 'top@gmail.ru',
        'role' => 'Student',
        'averange_mark' => 7.5
        

    ],
    [
        'full_name' => 'Keiko Agena',
        'phone' => '099-477-50-30',
        'email' => 'agena@gmail.ru',
        'role' => 'Student',
        'averange_mark' => 9.5
      

    ],
     
    [
        'full_name' => 'Sara Gison',
        'phone' => '098-123-18-11',
        'email' => 'gison@gmail.ru',
        'role' => 'Teacher',
        'subject' => 'Anatomy'
  
    ],
    [
        'full_name' => 'Angela Merkel',
        'phone' => '050-004-88-12',
        'email' => 'merkel@gmail.ru',
        'role' => 'Teacher',
        'subject' => 'Biology'
    

    ],
    [
        'full_name' => 'Hillary Clinton',
        'phone' => '098-454-44-37',
        'email' => 'clinton@gmail.ru',
        'role' => 'Teacher',
        'subject' => 'Englishe'
     

    ],
    [
        'full_name' => 'Dilma Rousseff', 
        'phone' => '070-757-88-39',
        'email' => 'rudty@gmail.ru',
        'role' => 'Teacher',
        'subject' => 'Computer techologies'
      
    ],
    [
        'full_name' => 'Olivia Colman', 
        'phone' => '070-000-88-39',
        'email' => 'colman@gmail.ru',
        'role' => 'Teacher',
        'subject' => 'Cybernetics'
      
    ],
    [
        'full_name' => 'Andra Day', 
        'phone' => '080-717-88-11',
        'email' => 'daen@gmail.ru',
        'role' => 'Teacher',
        'subject' => 'Economy'
      
    ],
    [
        'full_name' => 'Daniel Tison',
        'phone' => '068-454-55-12',
        'email' => 'tison@gmail.ru',
        'role' => 'Administrator',
         'working_day' => 'Monday'

    ],
    [
        'full_name' => 'Michelle Obama', 
        'phone' => '090-444-88-39',
        'email' => 'obama@gmail.ru',
        'role' => 'Administrator',
        'working_day' => 'Thursday'
      
    ],
    [
        'full_name' => ' Verónica Bachelet', 
        'phone' => '050-454-88-88',
        'email' => 'balchelet@gmail.ru',
        'role' => 'Administrator',
        'working_day' => 'Wednesday'
       
    ],
    [
        'full_name' => 'Maniela Robenson',
        'phone' => '068-444-45-14',
        'email' => 'robinson@gmail.ru',
        'role' => 'Administrator',
         'working_day' => 'Monday'

    ]
];

?>