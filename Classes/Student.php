<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/Classes/Person.php';

class Student extends Person{
    public $averange_mark = 0.0;

    public function __construct($id = null, $full_name, $phone, $email, $role, $averange_mark){
        parent::__construct($id = null, $full_name, $phone, $email, $role);
        $this -> averange_mark = $averange_mark;
    }
    
    public function getVisitCard(){
        return parent::getVisitCard()  . '<li><strong>Averange mark:</strong>' .$this -> averange_mark. '</li> ';
            }

      
    }
