<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/Classes/Person.php';

class Teacher extends Person{
    
    public $subject = '';

    public function __construct($id = null, $full_name, $phone, $email, $role, $subject){
        parent::__construct($id = null, $full_name, $phone, $email, $role);
        $this -> subject = $subject;
    }
    

    public function getVisitCard(){
     return parent::getVisitCard()  . '<li><strong>Subject:</strong>' .$this -> subject. '</li> ';
                    }
               
    }


?>