<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/Classes/Person.php';

class Administrator extends Person{
    public $working_day = '';
    
    public function __construct($id = null, $full_name, $phone, $email, $role, $working_day){
        parent::__construct($id = null, $full_name, $phone, $email, $role);
        $this -> working_day = $working_day;
    }
       
    public function getVisitCard(){
        return parent::getVisitCard()  . '<li><strong>Working_day:</strong>' .$this -> working_day. '</li> ';
            }
        
    }


?>