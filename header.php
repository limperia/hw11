
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info</title>
    <meta name="description" content="Info">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <header id="header" class="containar-fluid">
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="main_menu_brend">
                    <a class="navbar-brand" href="index.php">Info</a>
                </div>

                <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="create_db.php">Craeting a Table </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="connect.php">Connection</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="seeder_db.php">Fill with test data</a>
        </li>
        
      </ul>
    </div>


                <button class="navbar-toggler" type="button" name = "button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


            </nav>
        </div>

    </header>