<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/Array.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Classes/Person.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Classes/Administrator.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Classes/Teacher.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Classes/Student.php';


try {
    $sql = "SELECT * FROM members";
    $answerObject =  $connection->query($sql);
    $ar_member = $answerObject->fetchAll();
  
    $membersObject = [];

    foreach ($array_members as $ar_member) {
        switch ($ar_member['role']) {
            case 'Student':
                $membersObject[] = new  Student($ar_member['id'], $ar_member['full_name'], $ar_member['phone'], $ar_member['email'], $ar_member['role'], $ar_member['averange_mark']);
                break;
            case 'Administrator':
                $membersObject[] = new Administrator($ar_member['id'], $ar_member['full_name'], $ar_member['phone'], $ar_member['email'], $ar_member['role'], $ar_member['working_day']);
                break;
            case 'Teacher':
                $membersObject[] = new Teacher($ar_member['id'], $ar_member['full_name'], $ar_member['phone'], $ar_member['email'], $ar_member['role'], $ar_member['subject']);
                break;
        }
    }
} catch (Exception $exe_error) {
    die('Oшибка получения members!!!!<br>' . $exe_error->getMessage());
}


?>

<?php include 'header.php'; ?>

<section id="tables_main">
    <div class="containar-fluid">
        <div class="container">

            <div class="row">
           
                <?php foreach ($membersObject as  $membersObjects) : ?>
                  
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                
                        <div class="booking">
                   
                            <ul>
                                <li><?= $membersObjects->getVisitCard(); ?></li>
                                                    
                            </ul>

                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>



<?php include 'footer.php'; ?>